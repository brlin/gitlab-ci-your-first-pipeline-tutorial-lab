# GitLab CI your first pipeline tutorial lab

This is a lab project for the ["Tutorial: Your first pipeline | Getting started | Use CI/CD to build your application | Use GitLab | GitLab Docs" GitLab tutorial](https://docs.gitlab.com/ee/ci/quick_start/).

## Reference

* [Tutorial: Your first pipeline | Getting started | Use CI/CD to build your application | Use GitLab | GitLab Docs](https://docs.gitlab.com/ee/ci/quick_start/)
